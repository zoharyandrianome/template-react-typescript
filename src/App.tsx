import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import './assets/scss/main.scss';

const App = () => (
  <div className="App">

    <div className="main">

      <p>
        This is the content of the page
      </p>

    </div>

  </div>
);

export default App;
